## Overview

The simple Apache Spark CSV processing tool.

## Features
    
   * Read a csv file from local filesystem;
   * Remove rows where any string column is a empty string or just spaces (“”);
   * Convert data-type and names of the columns as per user’s choice;
   * For a given column, provide profiling information.

## TODO
   * Add Excel-like `lookup` feature;
   * Add Spark Entry point;
   * Improve test coverage.
