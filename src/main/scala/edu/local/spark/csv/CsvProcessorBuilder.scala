package edu.local.spark.csv

import org.apache.spark.sql.SQLContext

trait NotConfigured

trait Configured

/**
  * Fluent API to build CsvProcessor instance based on different parameters.
  *
  * @param filePath - path to CSV file
  * @param context  - sqlContext
  * @tparam A - Configured or NotConfigured
  */
class CsvProcessorBuilder[A] private(filePath: String,
                                     context: SQLContext) {


  def path(filePath: String): CsvProcessorBuilder[NotConfigured] = {
    new CsvProcessorBuilder[NotConfigured](filePath, this.context)
  }

  def sqlContext(context: SQLContext): CsvProcessorBuilder[Configured] = {
    new CsvProcessorBuilder[Configured](this.filePath, context)
  }

  def buildLocalFS()(implicit ev: CsvProcessorBuilder[A] =:= CsvProcessorBuilder[Configured]): CsvProcessor = {
    LocalFSCsvProcessor(filePath)(context)
  }
}

case object CsvProcessorBuilder {
  def apply(): CsvProcessorBuilder[NotConfigured] = new CsvProcessorBuilder[NotConfigured](null, null)
}
