package edu.local.spark.csv

import edu.local.spark.csv.model.ColumnProfile
import edu.local.spark.csv.model.request.{TransformationRequest, TransformationRequestUnit}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, Dataset, SQLContext}

import scala.annotation.tailrec


case class CsvDataFrame(df: DataFrame) {

  def normalize: CsvDataFrame = {
    val filterCondition = df.columns.map(c => col(c) === "" || trim(col(c)) === "").reduce(_ || _)
    this.copy(df = df.filter(not(filterCondition)))
  }

  def transform(request: TransformationRequest): CsvDataFrame = {
    @tailrec
    def loop(_df: DataFrame, units: List[TransformationRequestUnit]): DataFrame = units match {
      case unit :: xs =>
        loop(
          _df.withColumn(
            unit.new_col_name,
            _df(unit.existing_col_name).cast(unit.new_data_type.sparkDataType)
          ).drop(unit.existing_col_name),
          xs
        )
      case Nil =>
        _df
    }

    val valuableColumns = df.columns.intersect(request.transformations.map(_.existing_col_name)).map(col)

    this.copy(df = loop(df.select(valuableColumns: _*), request.transformations))
  }

  def profileColumn(columnName: String): ColumnProfile = {
    val colSelect =
      df.select(columnName)
        .filter(not(isnull(col(columnName))))
        .groupBy(columnName)
        .agg(count(columnName))
        .collect()

    ColumnProfile(
      name = columnName,
      uniqueValues = colSelect.length,
      values = colSelect.map { r =>
        r.get(0).toString -> r.getLong(1)
      }.toMap
    )
  }

  def profiling(implicit sqlContext: SQLContext): Dataset[ColumnProfile] = {
    import sqlContext.implicits._
    val profiles = for {
      c <- df.columns.toList
    } yield {
      profileColumn(c)
    }
    sqlContext.sparkContext.parallelize(profiles).toDS()
  }
}
