package edu.local.spark.csv

import edu.local.spark.csv.reader.Reader
import org.apache.spark.sql.SQLContext


trait CsvProcessor {
  def readRaw: CsvDataFrame
}

private[csv] case class LocalFSCsvProcessor(filePath: String)(implicit sqlContext: SQLContext) extends CsvProcessor {
  override def readRaw: CsvDataFrame = CsvDataFrame(Reader.asDF(filePath))
}
