package edu.local.spark.csv.model


case class ColumnProfile(name: String, uniqueValues: Long, values: Map[String, Long])
