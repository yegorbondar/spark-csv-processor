package edu.local.spark.csv.model.request

import enumeratum._
import enumeratum.EnumEntry._
import org.apache.spark.sql.types._


sealed abstract class CellDataType(val sparkDataType: DataType) extends EnumEntry with Snakecase with Lowercase

object CellDataType extends Enum[CellDataType] with CirceEnum[CellDataType] {
  val values = findValues

  case object string extends CellDataType(StringType)

  case object integer extends CellDataType(IntegerType)

  case object date extends CellDataType(DateType)

  case object boolean extends CellDataType(BooleanType)

  case object double extends CellDataType(DoubleType)

}

case class TransformationRequestUnit(existing_col_name: String, new_col_name: String, new_data_type: CellDataType, date_expression: Option[String] = None)

case class TransformationRequest(transformations: List[TransformationRequestUnit])
