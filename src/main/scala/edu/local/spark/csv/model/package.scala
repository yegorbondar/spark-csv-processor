package edu.local.spark.csv

import io.circe._

package object model {
  implicit val columnProfileEncoder: Encoder[ColumnProfile] = Encoder.forProduct3("name", "uniqueValues", "values")(cp => (cp.name, cp.uniqueValues, cp.values))
}
