package edu.local.spark.csv.model

import io.circe._

package object request {
  implicit val transformationRequestUnitDecoder: Decoder[TransformationRequestUnit] = Decoder.forProduct4("existing_col_name", "new_col_name", "new_data_type", "date_expression")(TransformationRequestUnit.apply)

  implicit val transformationRequestDecoder: Decoder[TransformationRequest] = Decoder.forProduct1("transformations")(TransformationRequest.apply)
}
