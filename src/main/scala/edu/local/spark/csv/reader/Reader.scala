package edu.local.spark.csv.reader

import java.nio.file.Paths

import kantan.csv._
import kantan.csv.ops._
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Row, SQLContext}

object Reader {
  def asDF(path: String)(implicit sqlContext: SQLContext): DataFrame = {
    val sc = sqlContext.sparkContext
    val rdd = sc.textFile(Paths.get(this.getClass.getResource(path).toURI).toString)
    val header = rdd.first().split(",").to[List]

    val columns = header.map { colName =>
      StructField(colName, StringType, nullable = true)
    }
    val schema = StructType(columns)

    val data = rdd.mapPartitionsWithIndex((i, it) => if (i == 0) it.drop(1) else it)
      .map {
        line =>
          line.asCsvReader[List[String]](rfc.withoutHeader).map(obj => obj.toEither).map(eth => eth.right.get).toList.head
      }.map(Row.fromSeq)

    sqlContext.createDataFrame(data, schema)
  }
}
