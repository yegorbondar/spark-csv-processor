package edu.local.spark

import edu.local.spark.csv.CsvProcessorBuilder
import edu.local.spark.csv.model.ColumnProfile
import edu.local.spark.csv.model.request.CellDataType._
import edu.local.spark.csv.model.request.{TransformationRequest, TransformationRequestUnit}
import edu.local.spark.util.SparkSpec
import org.apache.spark.sql.types.IntegerType
import org.scalatest.{FlatSpec, GivenWhenThen, Matchers}


class LocalFSCsvProcessorSpec extends FlatSpec with SparkSpec with GivenWhenThen with Matchers {

  "CsvDataFrame" should "correctly 'normalize' dataframe" in {
    Given("processor instance")
    val processor = CsvProcessorBuilder().path("/df/initial_df.csv").sqlContext(sqlContext).buildLocalFS()

    When("Request Spark SQL DataFrame from CSV file")
    val csvDF = processor.readRaw.normalize

    Then("Size should be the same as in CSV file")
    val result = evaluateDf[String](csvDF.df)
    result should have size 8
    result.head should have size 12
    result.head("RNCID") shouldBe "null"
  }

  it should "correctly process transformation request" in {
    Given("processor instance")
    val processor = CsvProcessorBuilder().path("/df/initial_df.csv").sqlContext(sqlContext).buildLocalFS()

    When("read csv file & normalize")
    val csvDF = processor.readRaw.normalize

    Then("transformation request correctly case all types and omit columns")
    val result = csvDF.transform(
      TransformationRequest(TransformationRequestUnit("RNCID", "rnc_id", integer) :: Nil)
    )

    result.df.schema.fields should have size 1

    result.df.schema.fields.head.dataType shouldBe IntegerType

    val _res = evaluateDf[Int](result.df)

    _res should have size 8
  }

  it should "correctly collect profiling information" in {
    Given("processor instance")
    val processor = CsvProcessorBuilder().path("/df/initial_df.csv").sqlContext(sqlContext).buildLocalFS()

    When("read csv file & normalize")
    val csvDF = processor.readRaw.normalize

    Then("transformation request correctly case all types and omit columns")
    val result = csvDF.profiling.collect().toList

    result should have size 12

    result.find(_.name == "RNCID") shouldBe Some(
      ColumnProfile(
        "RNCID",
        7,
        Map("310" -> 1, "302" -> 2, "303" -> 1, "306" -> 1, "null" -> 1, "309" -> 1, "305" -> 1)
      )
    )
  }

}
