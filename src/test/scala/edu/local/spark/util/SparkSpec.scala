package edu.local.spark.util

import com.holdenkarau.spark.testing.SharedSparkContext
import org.apache.spark.sql.{DataFrame, SQLContext}
import org.scalatest.Suite


trait SparkSpec extends SharedSparkContext {
  this: Suite =>

  implicit lazy val sqlContext = new SQLContext(sc)

  def evaluateDf[T](df: DataFrame, takeRows: Option[Int] = None): List[Map[String, T]] = {
    takeRows.fold(df.collect())(d => df.take(d)).map { row =>
      val fields = row.schema.toList.map(_.name)
      row.getValuesMap(fields)
    }.toList
  }

  def time[R](block: => R): R = {
    val t0 = System.currentTimeMillis()
    val result = block
    // call-by-name
    val t1 = System.currentTimeMillis()
    println("Elapsed time: " + (t1 - t0) / 1000 + " seconds")
    result
  }

}
