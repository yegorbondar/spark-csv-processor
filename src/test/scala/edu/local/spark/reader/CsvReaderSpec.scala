package edu.local.spark.reader

import edu.local.spark.csv.reader.Reader
import edu.local.spark.util.SparkSpec
import org.scalatest.{FlatSpec, GivenWhenThen, Matchers}

class CsvReaderSpec extends FlatSpec with SparkSpec with GivenWhenThen with Matchers {
  "Reader" should "correctly load content of the local CSV file" in {
    When("Request Spark SQL DataFrame from CSV file")
    val dataFrame = Reader.asDF("/df/initial_df.csv").cache()

    Then("Size should be the same as in CSV file")
    dataFrame.count() shouldBe 11
  }
}
