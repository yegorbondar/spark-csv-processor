val sparkVersion = "2.2.1"

val circeVersion = "0.8.0"

lazy val csv_spark_processor = (project in file(".")).
  enablePlugins(AssemblyPlugin, ReleasePlugin).
  configs(IntegrationTest).
  settings(Defaults.itSettings).
  settings(

    organization := "edu.local.spark",
    name := "csv-processor",
    version := "0.0.1",
    scalaVersion := "2.11.8",

    credentials += Credentials(Path.userHome / ".sbt" / "credentials"),

    assemblyMergeStrategy in assembly := {
      case PathList("META-INF", xs@_*) => MergeStrategy.discard
      case _ => MergeStrategy.first
    },

    mainClass in Compile := Some("edu.local.spark.csv.ProcessorMain"),
    assemblyJarName in assembly := s"csv-spark-processor-${version.value}.jar",

    //JSON support
    libraryDependencies ++= Seq(
      "io.circe" %% "circe-core",
      "io.circe" %% "circe-generic",
      "io.circe" %% "circe-parser"
    ).map(_ % circeVersion),

    addCompilerPlugin(
      "org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full
    ),

    //Spark dependencies
    libraryDependencies ++= Seq(
      "org.apache.spark" %% "spark-core",
      "org.apache.spark" %% "spark-sql"
    ).map(_ % sparkVersion).map {
      _ excludeAll(
        ExclusionRule(organization = "log4j"),
        ExclusionRule(organization = "org.slf4j")
      )
    },

    //util dependencies
    libraryDependencies ++= Seq(
      "com.nrinaudo" %% "kantan.csv" % "0.2.1",
      "com.iheart" %% "ficus" % "1.4.3",
      "joda-time" % "joda-time" % "2.9.9",
      "io.github.lukehutch" % "fast-classpath-scanner" % "2.0.18",
      "com.beachape" %% "enumeratum-circe" % "1.5.12",
      "org.scalactic" %% "scalactic" % "3.0.0"
    ),

    //logging
    libraryDependencies ++= Seq(
      "log4j" % "log4j" % "1.2.17" % "provided"
    ),

    //Test
    libraryDependencies ++= Seq(
      "com.holdenkarau" %% "spark-testing-base" % "2.2.1_0.9.0" % Test,
      "org.scalatest" %% "scalatest" % "3.0.0" % Test,
      "org.scalacheck" %% "scalacheck" % "1.13.4" % Test,
      "com.storm-enroute" %% "scalameter-core" % "0.8" % Test,
      "org.scalamock" %% "scalamock-scalatest-support" % "3.3.0" % Test
    ),

    fork in Test := false,
    parallelExecution in Test := false,
    javaOptions ++= Seq("-Xms512M", "-Xmx2048M", "-XX:MaxPermSize=2048M", "-XX:+CMSClassUnloadingEnabled")
  )
